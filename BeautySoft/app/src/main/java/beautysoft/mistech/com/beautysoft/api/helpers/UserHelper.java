package beautysoft.mistech.com.beautysoft.api.helpers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import beautysoft.mistech.com.beautysoft.api.Controller;
import beautysoft.mistech.com.beautysoft.api.IControllerListener;
import beautysoft.mistech.com.beautysoft.api.controllers.UserController;
import beautysoft.mistech.com.beautysoft.common.Constants;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class UserHelper extends Controller {

    private static final String TAG = "LookupHelper";
    private  static IControllerListener mCallback;

    public UserHelper(Context c, IControllerListener callback)
    {
        super(c,  callback);
        mCallback = callback;
    }

    public void GetClients(final String UserID, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    UserController lController = new UserController(mContext,mCallback);
                    return lController.GetClients(UserID);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.GETCLIENTS);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void GetAllStylistsForClient(final String UserID, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    UserController lController = new UserController(mContext,mCallback);
                    return lController.GetAllStylistsForClient(UserID);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.GETALLSTYLISTSFORCLIENT);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void UserDeleteClientAssociation(final String UserID,final String ClientID, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    UserController lController = new UserController(mContext,mCallback);
                    return lController.UserDeleteClientAssociation(UserID, ClientID);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.USERDELETECLIENTASSOCIATION);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void UserScanClientQR(final String UserID,final String QRGuid, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    UserController lController = new UserController(mContext,mCallback);
                    return lController.UserScanClientQR(UserID, QRGuid);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.USERSCANCLIENTQR);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void ClientUpdateBooking(final String BookingID,final String Status, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    UserController lController = new UserController(mContext,mCallback);
                    return lController.ClientUpdateBooking(BookingID, Status);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.CLIENTUPDATEBOOKING);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void GetCurrentUserInfo(final String UserID, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    UserController lController = new UserController(mContext,mCallback);
                    return lController.GetCurrentUserInfo(UserID);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.GETCURRENTUSERINFO);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void UpdateUserProfilePicture(final String UserID,final String ImageData, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    UserController lController = new UserController(mContext,mCallback);
                    return lController.UpdateUserProfilePicture(UserID,ImageData );
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.UPDATEUSERPROFILEPIC);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void UpdateUserInfo(final String UserID,final String FirstName,final String LastName,final String MobileNumber,final String EmailAddress, final String MaxBookings, final String ReceiveNotifications, final String ReceiveOffers, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    UserController lController = new UserController(mContext,mCallback);
                    return lController.UpdateUserInfo(UserID, FirstName, LastName, MobileNumber,EmailAddress, MaxBookings, ReceiveNotifications, ReceiveOffers);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.UPDATEUSERINFO);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void UpdateUserPassword(final String UserID,final String Password, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    UserController lController = new UserController(mContext,mCallback);
                    return lController.UpdateUserPassword(UserID, Password);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.UPDATEUSERPASSWORD);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void GetUserIsStylist(final String UserID, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    UserController lController = new UserController(mContext,mCallback);
                    return lController.GetUserIsStylist(UserID);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.GETUSERISSTYLIST);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void GetUserLoyaltyPoints(final String UserID, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    UserController lController = new UserController(mContext,mCallback);
                    return lController.GetUserLoyaltyPoints(UserID);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.GETUSERLOYALTY);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void GetUserVouchers(final String UserID, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    UserController lController = new UserController(mContext,mCallback);
                    return lController.GetUserVouchers(UserID);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.GETUSERVOUCHERS);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void UserScanVoucherRedeem(final String UserID,final String QRGuid, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    UserController lController = new UserController(mContext,mCallback);
                    return lController.UserScanVoucherRedeem(UserID, QRGuid);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.GETUSERVOUCHERREDEEM);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void GetUserNotifications(final String UserID, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    UserController lController = new UserController(mContext,mCallback);
                    return lController.GetUserNotifications(UserID);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.GETUSERNOTIFICATION);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void MarkNotificationAsRead(final String NotificationID, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    UserController lController = new UserController(mContext,mCallback);
                    return lController.MarkNotificationAsRead(NotificationID);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.MARKETNOTIFICATION);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void DeleteNotification(final String NotificationID, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    UserController lController = new UserController(mContext,mCallback);
                    return lController.DeleteNotification(NotificationID);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.DELETENOTIFICATION);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void UserCheckInOut(final String UserID,final String QRGuid, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    UserController lController = new UserController(mContext,mCallback);
                    return lController.UserCheckInOut(UserID, QRGuid);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.USERCHECKINANDOUT);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }
}
