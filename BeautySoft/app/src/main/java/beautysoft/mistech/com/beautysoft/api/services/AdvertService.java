package beautysoft.mistech.com.beautysoft.api.services;

import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;

/**
 * Created by George on 11/21/2017.
 */

public interface AdvertService {

    @FormUrlEncoded
    @POST("/Advert/GetOffers")
    Response GetOffers(@Field("UserID") String UserID);

    @FormUrlEncoded
    @GET("/Advert/GetAdverts")
    Response GetAdverts(@Field("Advert") String Advert);

}
