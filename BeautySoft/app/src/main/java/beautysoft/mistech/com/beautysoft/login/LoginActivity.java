package beautysoft.mistech.com.beautysoft.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import beautysoft.mistech.com.beautysoft.MainActivity;
import beautysoft.mistech.com.beautysoft.R;
import beautysoft.mistech.com.beautysoft.api.IControllerListener;
import beautysoft.mistech.com.beautysoft.api.helpers.LoginHelper;
import beautysoft.mistech.com.beautysoft.common.Constants;

/**
 * Created by George on 11/21/2017.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, IControllerListener {

    public static String TAG = "LoginActivity";

    //region Private Members

    Context mContext;
    LoginHelper loginHelper;

    //endregion

    //region Layout Members

    private EditText txtUserName;
    private EditText txtPassword;
    private Button btnLogin;

    //endregion

    //region Public Overrides

    @Override
    public void onCreate(Bundle savedInstance)
    {
        super.onCreate(savedInstance);
        mContext = this;
        setContentView(R.layout.activity_login);
        txtUserName = findViewById(R.id.txtEmail);
        txtPassword = findViewById(R.id.txtPassword);
        loginHelper = new LoginHelper(this.getApplicationContext(), this);
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btnLogin:
                doLogin();
        }
    }



    @Override
    public void onResponseReceived(JSONObject response) throws JSONException
    {
        if(response.getInt(Constants.API_RESPONSE_INDEX) == ResponseIndex.LOGINRESPONSE.ordinal())
        {
            if(response.getString("Status").equals("0"))
            {
                Snackbar.make(findViewById(R.id.snackbar), "SUCCESS", Snackbar.LENGTH_LONG);
            }
        }

    }

    @Override
    public void onNoConnectivity() {
        displayDialog();
    }

    private void displayDialog(){

//        getActivity().runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        getActivity().finish();
//                    }
//                };
//
//                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
//                builder.setTitle(R.string.NoConnectionTitle)
//                        .setMessage(R.string.NoConnectionBody)
//                        .setPositiveButton(R.string.ok,listener)
//                        .setCancelable(false)
//                        .show();
//            }
//        });
    }

    //endregion

    //region Private Methods

    private void doLogin()
    {
        String userName = txtUserName.getText().toString();
        String password = txtPassword.getText().toString();
        loginHelper.loginUser(userName, password, "ANDROID", "ANDROID", this);

        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }

    //endregion
}
