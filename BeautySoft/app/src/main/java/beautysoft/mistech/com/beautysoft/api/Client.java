package beautysoft.mistech.com.beautysoft.api;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import beautysoft.mistech.com.beautysoft.api.services.AdvertService;
import beautysoft.mistech.com.beautysoft.api.services.BookingService;
import beautysoft.mistech.com.beautysoft.api.services.LoginService;
import beautysoft.mistech.com.beautysoft.api.services.UserService;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by George on 11/21/2017.
 */

public class Client {

    public static final String ApiUrl = "http://197.242.147.145/BSMVCWebService/api";
    public static final String TAG = "APIClient";

    private IControllerListener mCallback;
    private ConnectivityAwareUrlClient cauc;

    private UserService userService;
    private LoginService loginService;
    private AdvertService advertService;
    private BookingService bookingService;

    public Client(Context c, IControllerListener callback)
    {
        mCallback = callback;
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(150, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(150, TimeUnit.SECONDS);
        okHttpClient.setWriteTimeout(150, TimeUnit.SECONDS);

        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(new ItemTypeAdapterFactory())
                .create();
        cauc = new ConnectivityAwareUrlClient(c, new OkClient(okHttpClient), new NetworkConnectivityManager(c),mCallback);

        final RestAdapter Adapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(ApiUrl)
                .setConverter(new GsonConverter(gson))
                .setClient(new OkClient(okHttpClient))
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addHeader("Accept", "application/json");
                    }
                }).build();

        userService = Adapter.create(UserService.class);
        loginService = Adapter.create(LoginService.class);
        advertService = Adapter.create(AdvertService.class);
        bookingService = Adapter.create(BookingService.class);
    }

    public void setIControllerListner(IControllerListener callBack){
        mCallback = callBack;
        cauc.setIControllerListener(mCallback);
    }

    public UserService getUserService()
    {
        return userService;
    }

    public LoginService getLoginService()
    {
        return loginService;
    }

    public AdvertService getAdvertService()
    {
        return advertService;
    }

    public BookingService getBookingService()
    {
        return bookingService;
    }
}
