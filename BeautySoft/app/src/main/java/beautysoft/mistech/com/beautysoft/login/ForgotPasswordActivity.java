package beautysoft.mistech.com.beautysoft.login;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import beautysoft.mistech.com.beautysoft.R;

/**
 * Created by George on 11/21/2017.
 */

public class ForgotPasswordActivity extends AppCompatActivity {

    public static String TAG = "ForgotPasswordActivity";

    @Override
    public void onCreate(Bundle savedInstance)
    {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_forgotpassword);
    }
}
