package beautysoft.mistech.com.beautysoft;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import beautysoft.mistech.com.beautysoft.login.LoginActivity;

/**
 * Created by George on 11/21/2017.
 */

public class StartActivity extends AppCompatActivity implements View.OnClickListener{

    public static String TAG = "StartActivity";

    Context mContext;

    Button btnLogin;


    @Override
    public void onCreate(Bundle savedInstance)
    {
        super.onCreate(savedInstance);
        mContext = this;
        setContentView(R.layout.activity_start);
        btnLogin = findViewById((R.id.btnLogin));
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btnLogin:
                openLogin();
            case R.id.btnRegister:
                openRegister();
        }
    }

    public void openLogin()
    {
        Intent intent = new Intent(mContext, LoginActivity.class);
        startActivity(intent);
    }

    public void openRegister()
    {
/*        Intent intent = new Intent (this, RegistrationActivity.class);
        startActivity(intent);*/
    }
}
