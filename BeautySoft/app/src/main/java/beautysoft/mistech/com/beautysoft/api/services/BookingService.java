package beautysoft.mistech.com.beautysoft.api.services;

import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by George on 11/21/2017.
 */

public interface BookingService {

    @FormUrlEncoded
    @POST("/Booking/GetUserBookings")
    Response GetUserBookings(@Field("UserID") String UserID);

    @FormUrlEncoded
    @POST("/Booking/GetUserHistoricBookings")
    Response GetUserHistoricBookings(@Field("UserID") String UserID);

    @FormUrlEncoded
    @POST("/Booking/GetCompanyServices")
    Response GetCompanyServices(@Field("UserID") String ClientID);

    @FormUrlEncoded
    @POST("/Booking/GetCompanyDisabledWorkDays")
    Response GetCompanyDisabledWorkDays(@Field("UserID") String CompanyID);

    @FormUrlEncoded
    @POST("/Booking/GetAllBookingsForStylistDay")
    Response GetAllBookingsForStylistDay(@Field("UserID") String UserID
            , @Field("DateToGet") String DateToGet);

    @FormUrlEncoded
    @POST("/Booking/CheckStylistOpeningsForDay")
    Response CheckStylistOpeningsForDay(@Field("StylistUserID") String StylistUserID
            , @Field("CompanyID") String CompanyID
            , @Field("SelectedServices") String SelectedServices
            , @Field("ProposedDate") String ProposedDate);

    @FormUrlEncoded
    @POST("/Booking/ClientCreateTentativeBooking")
    Response ClientCreateTentativeBooking(@Field("UserID") String UserID
            , @Field("ClientID") String ClientID
            , @Field("StylistID") String StylistID
            , @Field("DateTime") String DateTime
            , @Field("ServiceIDs") String ServiceIDs
            , @Field("ProductIDs") String ProductIDs
            , @Field("SuggestedStyleImage") String SuggestedStyleImage);

    @FormUrlEncoded
    @POST("/Booking/GetBookingDetails")
    Response GetBookingDetails(@Field("BookingID") String BookingID);

    @FormUrlEncoded
    @POST("/Booking/StylistUpdateConfirmBooking")
    Response StylistUpdateConfirmBooking(@Field("BookingID") String BookingID
            , @Field("BookingDateTime") String BookingDateTime);

    @FormUrlEncoded
    @POST("/Booking/StylistCancelBooking")
    Response StylistCancelBooking(@Field("BookingID") String BookingID);

    @FormUrlEncoded
    @POST("/Booking/GetCustomersByName")
    Response GetCustomersByName(@Field("CompanyID") String CompanyID
            , @Field("CustomerNameString") String CustomerNameString);

    @FormUrlEncoded
    @POST("/Booking/StylistCreateBooking")
    Response StylistCreateBooking(@Field("StylistID") String StylistID
            , @Field("StartDateTime") String StartDateTime
            , @Field("CompanyID") String CompanyID
            , @Field("CustomerID") String CustomerID
            , @Field("ServiceIDs") String ServiceIDs);

    @FormUrlEncoded
    @POST("/Booking/ClientConfirmBooking")
    Response ClientConfirmBooking(@Field("BookingID") String BookingID);

    @FormUrlEncoded
    @POST("/Booking/ClientCancelBooking")
    Response ClientCancelBooking(@Field("BookingID") String BookingID);



}
