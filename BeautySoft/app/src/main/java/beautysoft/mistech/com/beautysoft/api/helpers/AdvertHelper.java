package beautysoft.mistech.com.beautysoft.api.helpers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import beautysoft.mistech.com.beautysoft.api.Controller;
import beautysoft.mistech.com.beautysoft.api.IControllerListener;
import beautysoft.mistech.com.beautysoft.api.controllers.AdvertController;
import beautysoft.mistech.com.beautysoft.common.Constants;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class AdvertHelper extends Controller {

    private static final String TAG = "LookupHelper";
    private  static IControllerListener mCallback;

    public AdvertHelper(Context c, IControllerListener callback)
    {
        super(c,  callback);
        mCallback = callback;
    }

    public void GetOffers(final String UserID, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    AdvertController lController = new AdvertController(mContext,mCallback);
                    return lController.GetOffers(UserID);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.GETOFFERS);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }


}
