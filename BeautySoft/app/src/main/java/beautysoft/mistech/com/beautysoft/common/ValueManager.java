package beautysoft.mistech.com.beautysoft.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by George on 11/21/2017.
 */

public class ValueManager
{
    private Context mContext;
    private final SharedPreferences mPrefs;

    public ValueManager(Context c)
    {
        mContext = c;
        mPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    public void put(String key, String value)
    {
        putString(key, value);
    }

    public void putString(String key, String value)
    {
        mPrefs.edit().putString(key, value).commit();
    }

    public void putInt(String key, int value)
    {
        mPrefs.edit().putInt(key, value).commit();
    }

    public void putLong(String key, long value)
    {
        mPrefs.edit().putLong(key, value).commit();
    }

    public void putDouble(String key, double value)
    {
        mPrefs.edit().putLong(key,  Double.doubleToRawLongBits(value)).commit();
    }

    public void putBoolean(String key, Boolean value)
    {
        mPrefs.edit().putBoolean(key, value).commit();
    }

    public String getString(String key)
    {
        return mPrefs.getString(key, "");
    }

    public int getInt(String key)
    {
        return mPrefs.getInt(key, 0);
    }

    public long getLong(String key)
    {
        return mPrefs.getLong(key, 0);
    }

    public double getDouble(String key)
    {
        return Double.longBitsToDouble(mPrefs.getLong(key, Double.doubleToLongBits(0.0)));
    }

    public Boolean getBoolean(String key)
    {
        return mPrefs.getBoolean(key, false);
    }

    public void clear()
    {
        mPrefs.edit().clear().commit();
    }

}
