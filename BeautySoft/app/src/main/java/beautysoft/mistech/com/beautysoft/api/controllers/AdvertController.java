package beautysoft.mistech.com.beautysoft.api.controllers;

import android.content.Context;

import org.json.JSONException;

import beautysoft.mistech.com.beautysoft.api.Controller;
import beautysoft.mistech.com.beautysoft.api.IControllerListener;
import retrofit.client.Response;

public class AdvertController extends Controller {

    private static final String TAG = "CUserController";

    public AdvertController(Context c, IControllerListener callback)
    {
        super(c, callback);
    }

    public Response GetOffers(String UserID) throws JSONException
    {
        Response response;
        try {
            response = getClient().getAdvertService().GetOffers(UserID);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }


}
