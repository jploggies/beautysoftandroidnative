package beautysoft.mistech.com.beautysoft.api;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by George on 11/21/2017.
 */

public interface IControllerListener
{
//    public void setResponseIndex(ResponseIndex val);
//    public ResponseIndex getResponseIndex();
//    public void setKey(String key);
//    public String getKey();
    public void onResponseReceived(JSONObject response) throws JSONException;
    public void onNoConnectivity();
    public static enum ResponseIndex
    {
        NO_CONNECTION
        ,SERVER_ERROR
        ,LOGINRESPONSE
        ,VERIFICATIONIDANDTOKENRESPONSE
        ,REGISTERUSER
        ,FORGOTPASSWORD
        ,GETUSERNAMENOTUSED
        ,GETEMAILADDRESSNOTUSED
        ,RESETPASSWORDSUBMIT
        ,GETOFFERS
        ,GETADVERT
        ,GETCLIENTS
        ,GETALLSTYLISTSFORCLIENT
        ,USERDELETECLIENTASSOCIATION
        ,USERSCANCLIENTQR
        ,CLIENTUPDATEBOOKING
        ,GETCURRENTUSERINFO
        ,UPDATEUSERPROFILEPIC
        ,UPDATEUSERINFO
        ,UPDATEUSERPASSWORD
        ,GETUSERISSTYLIST
        ,GETUSERLOYALTY
        ,GETUSERVOUCHERS
        ,GETUSERVOUCHERREDEEM
        ,GETUSERNOTIFICATION
        ,MARKETNOTIFICATION
        ,DELETENOTIFICATION
        ,USERCHECKINANDOUT
        ,GETUSERBOOKING
        ,GETUSERHISTORICBOOKINGS
        ,GETCOMPANYSERVICES
        ,GETCOMPANYDISABLEDWORKDAYS
        ,GETALLBOOKINGSFORSTYLISTDAY
        ,CHECKSTYLISTOPENINGDAYS
        ,CLIENTCREATETENTIVEBOOKING
        ,GETBOOKINGDETAILS
        ,STYLISTCONFIRMUPDATEBOOKING
        ,STYLISTCANCELBOOKING
        ,GETCUSTOMERBYNAME
        ,STYLISTCREATEBOOKING
        ,CLIENTCONFIRMBOOKING
        ,CLIENTCANCELBOOKING
    }
}
