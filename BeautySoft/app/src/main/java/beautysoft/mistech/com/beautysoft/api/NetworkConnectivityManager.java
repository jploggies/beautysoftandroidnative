package beautysoft.mistech.com.beautysoft.api;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by George on 11/21/2017.
 */

public class NetworkConnectivityManager {
    Context context;
    static String TAG = "NetworkConnectManager";
    public static String SERVER_RESPO_200 = "200";

    public NetworkConnectivityManager(Context context) {
        this.context = context;
    }

    /** Check network connectivity **/
    public boolean hasDataConnectivity() {
        boolean res = false;
        try {
            final ConnectivityManager conMgr = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
            if (activeNetwork != null && activeNetwork.isConnected()) {
                // user is online
                res = true;
            } else {
                // user is offline
                res = false;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return res;
    }

}
