package beautysoft.mistech.com.beautysoft.api;

import android.content.Context;

import beautysoft.mistech.com.beautysoft.common.ValueManager;

/**
 * Created by George on 11/21/2017.
 */

public class Controller
{
    protected static Context mContext = null;
    protected final ValueManager mPrefs;
    private static Client client;
    private static final String TAG = "Controller";
    private static IControllerListener callbackListener;

    public Controller(Context c, IControllerListener callback)
    {
        mPrefs = new ValueManager(c);
        mContext = c;
        callbackListener = callback;
    }

    public static Client getClient()
    {

        if (client == null)
        {
            client = new Client(mContext,callbackListener);
        }
        client.setIControllerListner(callbackListener);
        return client;
    }
}
