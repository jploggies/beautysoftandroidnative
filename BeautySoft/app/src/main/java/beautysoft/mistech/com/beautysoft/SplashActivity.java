package beautysoft.mistech.com.beautysoft;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by George on 11/21/2017.
 */

public class SplashActivity extends AppCompatActivity {

    public static String TAG = "SplashActivity";

    CountDownTimer timer;
    Context mContext;

    @Override
    public void onCreate(Bundle savedInstance)
    {
        super.onCreate(savedInstance);
        mContext = this;
        setContentView(R.layout.activity_splash);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        startTimer();
    }

    private void startTimer()
    {
        timer = new CountDownTimer(3000, 1000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                Intent intent = new Intent(mContext, StartActivity.class);
                startActivity(intent);
                timer.onFinish();
//                this.onFinish();
            }
        };
        timer.start();
    }
}
