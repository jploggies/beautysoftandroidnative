package beautysoft.mistech.com.beautysoft.api.controllers;

import android.content.Context;

import org.json.JSONException;

import beautysoft.mistech.com.beautysoft.api.Controller;
import beautysoft.mistech.com.beautysoft.api.IControllerListener;
import retrofit.client.Response;

public class UserController extends Controller {

    private static final String TAG = "CUserController";

    public UserController(Context c, IControllerListener callback)
    {
        super(c, callback);
    }

    public Response GetClients(String UserID) throws JSONException
    {
        Response response;
        try {
            response = getClient().getUserService().GetClients(UserID);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response GetAllStylistsForClient(String UserID) throws JSONException
    {
        Response response;
        try {
            response = getClient().getUserService().GetAllStylistsForClient(UserID);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response UserDeleteClientAssociation(String UserID, String ClientID) throws JSONException
    {
        Response response;
        try {
            response = getClient().getUserService().UserDeleteClientAssociation(UserID
            ,ClientID);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response UserScanClientQR(String UserID, String QRGuid) throws JSONException
    {
        Response response;
        try {
            response = getClient().getUserService().UserScanClientQR(UserID,
                    QRGuid);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response ClientUpdateBooking(String BookingID, String Status) throws JSONException
    {
        Response response;
        try {
            response = getClient().getUserService().ClientUpdateBooking(BookingID
                    , Status);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response GetCurrentUserInfo(String UserID) throws JSONException
    {
        Response response;
        try {
            response = getClient().getUserService().GetCurrentUserInfo(UserID);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response UpdateUserProfilePicture(String UserID, String ImageData) throws JSONException
    {
        Response response;
        try {
            response = getClient().getUserService().UpdateUserProfilePicture(UserID
                    , ImageData);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response UpdateUserInfo(String UserID, String FirstName, String LastName, String MobileNumber, String EmailAddress, String MaxBookings, String ReceiveNotifications, String ReceiveOffers) throws JSONException
    {
        Response response;
        try {
            response = getClient().getUserService().UpdateUserInfo(UserID
                    , FirstName
                    , LastName
                    , MobileNumber
                    , EmailAddress
                    , MaxBookings
                    , ReceiveNotifications
                    , ReceiveOffers);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response UpdateUserPassword(String UserID, String Password) throws JSONException
    {
        Response response;
        try {
            response = getClient().getUserService().UpdateUserPassword(UserID
                    , Password);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response GetUserIsStylist(String UserID) throws JSONException
    {
        Response response;
        try {
            response = getClient().getUserService().GetUserIsStylist(UserID
                     );
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response GetUserLoyaltyPoints(String UserID) throws JSONException
    {
        Response response;
        try {
            response = getClient().getUserService().GetUserLoyaltyPoints(UserID);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response GetUserVouchers(String UserID) throws JSONException
    {
        Response response;
        try {
            response = getClient().getUserService().GetUserVouchers(UserID);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response UserScanVoucherRedeem(String UserID,String QRGuid) throws JSONException
    {
        Response response;
        try {
            response = getClient().getUserService().UserScanVoucherRedeem(UserID
            ,QRGuid);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response GetUserNotifications(String UserID ) throws JSONException
    {
        Response response;
        try {
            response = getClient().getUserService().GetUserNotifications(UserID);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response MarkNotificationAsRead(String NotificationID) throws JSONException
    {
        Response response;
        try {
            response = getClient().getUserService().MarkNotificationAsRead(NotificationID);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response DeleteNotification(String NotificationID) throws JSONException
    {
        Response response;
        try {
            response = getClient().getUserService().DeleteNotification(NotificationID);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response UserCheckInOut(String UserID, String QRGuid) throws JSONException
    {
        Response response;
        try {
            response = getClient().getUserService().UserCheckInOut(UserID
                    , QRGuid);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

}
