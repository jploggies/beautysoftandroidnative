package beautysoft.mistech.com.beautysoft.api.controllers;

import android.content.Context;

import org.json.JSONException;

import beautysoft.mistech.com.beautysoft.api.Controller;
import beautysoft.mistech.com.beautysoft.api.IControllerListener;
import retrofit.client.Response;

public class BookingController extends Controller{

    private static final String TAG = "CUserController";

    public BookingController(Context c, IControllerListener callback)
    {
        super(c, callback);
    }

    public Response GetUserBookings(String UserID) throws JSONException
    {
        Response response;
        try {
            response = getClient().getBookingService().GetUserBookings(UserID);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response GetUserHistoricBookings(String UserID) throws JSONException
    {
        Response response;
        try {
            response = getClient().getBookingService().GetUserHistoricBookings(UserID);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response GetCompanyServices(String ClientID) throws JSONException
    {
        Response response;
        try {
            response = getClient().getBookingService().GetCompanyServices(ClientID);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response GetCompanyDisabledWorkDays(String CompanyID) throws JSONException
    {
        Response response;
        try {
            response = getClient().getBookingService().GetCompanyDisabledWorkDays(CompanyID);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response GetAllBookingsForStylistDay(String UserID, String DateToGet) throws JSONException
    {
        Response response;
        try {
            response = getClient().getBookingService().GetAllBookingsForStylistDay(UserID
                , DateToGet);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response CheckStylistOpeningsForDay(String StylistUserID, String CompanyID, String SelectedServices, String ProposedDate) throws JSONException
    {
        Response response;
        try {
            response = getClient().getBookingService().CheckStylistOpeningsForDay(StylistUserID
                , CompanyID
                , SelectedServices
                , ProposedDate);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response ClientCreateTentativeBooking(String UserID, String ClientID, String StylistID, String DateTime, String ServiceIDs, String ProductIDs, String SuggestedStyleImage) throws JSONException
    {
        Response response;
        try {
            response = getClient().getBookingService().ClientCreateTentativeBooking(UserID
                    , ClientID
                    , StylistID
                    , DateTime
                    , ServiceIDs
                    , ProductIDs
                    , SuggestedStyleImage);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response GetBookingDetails(String BookingID) throws JSONException
    {
        Response response;
        try {
            response = getClient().getBookingService().GetBookingDetails(BookingID);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response StylistUpdateConfirmBooking(String BookingID, String BookingDateTime) throws JSONException
    {
        Response response;
        try {
            response = getClient().getBookingService().StylistUpdateConfirmBooking(BookingID
                , BookingDateTime);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response StylistCancelBooking(String BookingID) throws JSONException
    {
        Response response;
        try {
            response = getClient().getBookingService().StylistCancelBooking(BookingID);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response GetCustomersByName(String CompanyID, String CustomerNameString) throws JSONException
    {
        Response response;
        try {
            response = getClient().getBookingService().GetCustomersByName(CompanyID
                , CustomerNameString);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response StylistCreateBooking(String StylistID, String StartDateTime, String CompanyID, String CustomerID, String ServiceIDs) throws JSONException
    {
        Response response;
        try {
            response = getClient().getBookingService().StylistCreateBooking(StylistID
                , StartDateTime
                , CompanyID
                , CustomerID
                , ServiceIDs);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response ClientConfirmBooking(String BookingID) throws JSONException
    {
        Response response;
        try {
            response = getClient().getBookingService().ClientConfirmBooking(BookingID);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response ClientCancelBooking(String BookingID) throws JSONException
    {
        Response response;
        try {
            response = getClient().getBookingService().ClientCancelBooking(BookingID);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }


}
