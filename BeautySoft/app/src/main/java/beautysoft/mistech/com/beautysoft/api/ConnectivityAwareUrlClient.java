package beautysoft.mistech.com.beautysoft.api;

import android.content.Context;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import beautysoft.mistech.com.beautysoft.common.NoConnectivityException;
import retrofit.client.*;

/**
 * Created by George on 11/21/2017.
 */

public class ConnectivityAwareUrlClient implements retrofit.client.Client {
    IControllerListener mListener;
    Context mContext;
    public ConnectivityAwareUrlClient(Context c, retrofit.client.Client wrappedClient, NetworkConnectivityManager ncm, IControllerListener listener ) {
        mListener = listener;
        mContext = c;
        this.wrappedClient = wrappedClient;
        this.ncm = ncm;
    }

    retrofit.client.Client wrappedClient;
    private NetworkConnectivityManager ncm;

    @Override
    public Response execute(Request request) throws IOException {
        try {
            if (!ncm.hasDataConnectivity()) {
                throw new NoConnectivityException();
            }
        }catch (NoConnectivityException e){
            mListener.onNoConnectivity();
        }

        try {
            return wrappedClient.execute(request);
        }catch (UnknownHostException e) {
            mListener.onNoConnectivity();
            return null;
        }catch (SocketTimeoutException ste){
            mListener.onNoConnectivity();
            return null;
        }
    }

    public void setIControllerListener (IControllerListener callBack){
        mListener = callBack;
    }



}
