package beautysoft.mistech.com.beautysoft.api.services;

import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by George on 11/21/2017.
 */

public interface LoginService {

    @FormUrlEncoded
    @POST("/Login/LoginUser")
    Response LoginUser(@Field("UserName") String UserName
            , @Field("Password") String Password
            , @Field("DeviceToken") String cUserID
            , @Field("DevicePlatform") String publicModulus
            , @Field("RegistrationID") String AuthToken);

    @FormUrlEncoded
    @POST("/Login/VerifyUserIDAndToken")
    Response VerifyUserIDAndToken(@Field("UserID") String UserID
            , @Field("DeviceToken") String cUserID
            , @Field("DevicePlatform") String publicModulus
            , @Field("RegistrationID") String AuthToken);

    @FormUrlEncoded
    @POST("/Login/RegisterUser")
    Response RegisterUser(@Field("UserName") String UserName
            , @Field("FirstName") String FirstName
            , @Field("LastName") String LastName
            , @Field("Password") String Password
            , @Field("Email") String Email
            , @Field("CellNo") String CellNo
            , @Field("DeviceToken") String cUserID
            , @Field("DevicePlatform") String publicModulus
            , @Field("RegistrationID") String AuthToken
            , @Field("Gender") String Gender);

    @FormUrlEncoded
    @POST("/Login/ForgotPassword")
    Response ForgotPassword(@Field("UserName") String UserName
            , @Field("Email") String Email);

    @FormUrlEncoded
    @POST("/Login/GetUserNameNotUsed")
    Response GetUserNameNotUsed(@Field("UserName") String UserName);

    @FormUrlEncoded
    @POST("/Login/GetEmailAddressNotUsed")
    Response GetEmailAddressNotUsed(@Field("EmailAddress") String EmailAddress);


//    @GET("/v3/app_Lookup/get_CUserIsVerified")
//    Response get_CUserIsVerified(@Query("cUserID") String cUserID);
}
