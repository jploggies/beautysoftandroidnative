package beautysoft.mistech.com.beautysoft.api.helpers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import beautysoft.mistech.com.beautysoft.api.Controller;
import beautysoft.mistech.com.beautysoft.api.IControllerListener;
import beautysoft.mistech.com.beautysoft.api.controllers.BookingController;
import beautysoft.mistech.com.beautysoft.common.Constants;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class BookingHelper extends Controller {

    private static final String TAG = "LookupHelper";
    private  static IControllerListener mCallback;

    public BookingHelper(Context c, IControllerListener callback)
    {
        super(c,  callback);
        mCallback = callback;
    }

    public void GetUserBookings(final String UserID, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    BookingController lController = new BookingController(mContext,mCallback);
                    return lController.GetUserBookings(UserID);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.GETUSERBOOKING);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void GetUserHistoricBookings(final String UserID, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    BookingController lController = new BookingController(mContext,mCallback);
                    return lController.GetUserHistoricBookings(UserID);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.GETUSERHISTORICBOOKINGS);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void GetCompanyServices(final String ClientID,final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    BookingController lController = new BookingController(mContext,mCallback);
                    return lController.GetCompanyServices(ClientID);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.GETCOMPANYSERVICES);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void GetCompanyDisabledWorkDays(final String CompanyID, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    BookingController lController = new BookingController(mContext,mCallback);
                    return lController.GetCompanyDisabledWorkDays(CompanyID);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.GETCOMPANYDISABLEDWORKDAYS);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void GetAllBookingsForStylistDay(final String UserID, final String DateToGet, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    BookingController lController = new BookingController(mContext,mCallback);
                    return lController.GetAllBookingsForStylistDay(UserID, DateToGet);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.GETALLBOOKINGSFORSTYLISTDAY);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void CheckStylistOpeningsForDay(final String StylistUserID, final String CompanyID, final String SelectedServices, final String ProposedDate, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    BookingController lController = new BookingController(mContext,mCallback);
                    return lController.CheckStylistOpeningsForDay(StylistUserID, CompanyID, SelectedServices,ProposedDate);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.CHECKSTYLISTOPENINGDAYS);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void ClientCreateTentativeBooking(final String UserID, final String ClientID, final String StylistID, final String DateTime, final String ServiceIDs, final String ProductIDs, final String SuggestedStyleImage, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    BookingController lController = new BookingController(mContext,mCallback);
                    return lController.ClientCreateTentativeBooking(UserID, ClientID, StylistID, DateTime, ServiceIDs, ProductIDs, SuggestedStyleImage);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.CLIENTCREATETENTIVEBOOKING);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void GetBookingDetails(final String BookingID, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    BookingController lController = new BookingController(mContext,mCallback);
                    return lController.GetBookingDetails(BookingID);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.GETBOOKINGDETAILS);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void StylistUpdateConfirmBooking(final String BookingID, final String BookingDateTime, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    BookingController lController = new BookingController(mContext,mCallback);
                    return lController.StylistUpdateConfirmBooking(BookingID, BookingDateTime);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.STYLISTCONFIRMUPDATEBOOKING);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void StylistCancelBooking(final String BookingID, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    BookingController lController = new BookingController(mContext,mCallback);
                    return lController.StylistCancelBooking(BookingID);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.STYLISTCANCELBOOKING);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void GetCustomersByName(final String CompanyID, final String CustomerNameString, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    BookingController lController = new BookingController(mContext,mCallback);
                    return lController.GetCustomersByName(CompanyID, CustomerNameString);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.GETCUSTOMERBYNAME);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void StylistCreateBooking(final String StylistID, final String StartDateTime, final String CompanyID, final String CustomerID,final String ServiceIDs, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    BookingController lController = new BookingController(mContext,mCallback);
                    return lController.StylistCreateBooking(StylistID, StartDateTime, CompanyID, CustomerID, ServiceIDs);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.STYLISTCREATEBOOKING);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void ClientConfirmBooking(final String BookingID, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    BookingController lController = new BookingController(mContext,mCallback);
                    return lController.ClientConfirmBooking(BookingID);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.CLIENTCONFIRMBOOKING);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void ClientCancelBooking(final String BookingID, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    BookingController lController = new BookingController(mContext,mCallback);
                    return lController.ClientCancelBooking(BookingID);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.CLIENTCANCELBOOKING);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }


}
