package beautysoft.mistech.com.beautysoft.api.helpers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import beautysoft.mistech.com.beautysoft.api.Controller;
import beautysoft.mistech.com.beautysoft.api.IControllerListener;
import beautysoft.mistech.com.beautysoft.api.controllers.LoginController;
import beautysoft.mistech.com.beautysoft.common.Constants;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by George on 11/21/2017.
 */

public class LoginHelper extends Controller {

    private static final String TAG = "LookupHelper";
    private  static IControllerListener mCallback;

    public LoginHelper(Context c, IControllerListener callback)
    {
        super(c,  callback);
        mCallback = callback;
    }

    public void loginUser(final String UserName, final String Password, final String DeviceToken, final String RegistrationID, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    LoginController lController = new LoginController(mContext,mCallback);
                    return lController.loginUser(UserName, Password, DeviceToken, "Android", RegistrationID);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.LOGINRESPONSE);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void VerifyUserIDAndToken(final String UserID, final String DeviceToken, final String RegistrationID, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    LoginController lController = new LoginController(mContext,mCallback);
                    return lController.VerifyUserIDAndToken(UserID, DeviceToken, "Android", RegistrationID);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.VERIFICATIONIDANDTOKENRESPONSE);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void RegisterUser(final String UserName,final String FirstName, final String LastName,final String Password,final String Email,final String CellNo, final String DeviceToken, final String RegistrationID,final String Gender, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    LoginController lController = new LoginController(mContext,mCallback);
                    return lController.RegisterUser(UserName,FirstName,LastName, Password,Email,CellNo, DeviceToken, "Android", RegistrationID,Gender);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.REGISTERUSER);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void ForgotPassword(final String UserName,final String Email, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    LoginController lController = new LoginController(mContext,mCallback);
                    return lController.ForgotPassword(UserName, Email);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.FORGOTPASSWORD);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void GetUserNameNotUsed(final String UserName, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    LoginController lController = new LoginController(mContext,mCallback);
                    return lController.GetUserNameNotUsed(UserName);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.GETUSERNAMENOTUSED);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }

    public void GetEmailAddressNotUsed(final String EmailAddress, final IControllerListener listener)
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Response> task = new AsyncTask<Void, Void, Response>()
        {
            @Override
            protected Response doInBackground(Void... params)
            {
                try
                {
                    LoginController lController = new LoginController(mContext,mCallback);
                    return lController.GetEmailAddressNotUsed(EmailAddress);
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Response result)
            {
                try
                {
                    JSONObject resultObject = new JSONObject(new String(((TypedByteArray) result.getBody()).getBytes()));
                    resultObject.put(Constants.API_RESPONSE_INDEX, IControllerListener.ResponseIndex.GETEMAILADDRESSNOTUSED);
                    listener.onResponseReceived(resultObject);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };
        task.execute(null, null, null);
    }
}
