package beautysoft.mistech.com.beautysoft.api.services;

import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by George on 11/21/2017.
 */

public interface UserService {

    @FormUrlEncoded
    @POST("/User/GetClients")
    Response GetClients(@Field("UserID") String UserID);

    @FormUrlEncoded
    @POST("/User/GetAllStylistsForClient")
    Response GetAllStylistsForClient(@Field("UserID") String UserID);

    @FormUrlEncoded
    @POST("/User/UserDeleteClientAssociation")
    Response UserDeleteClientAssociation(@Field("UserID") String UserID
            , @Field("ClientID") String ClientID);

    @FormUrlEncoded
    @POST("/User/UserScanClientQR")
    Response UserScanClientQR(@Field("UserID") String UserID
            , @Field("QRGuid") String QRGuid);

    @FormUrlEncoded
    @POST("/User/ClientUpdateBooking")
    Response ClientUpdateBooking(@Field("BookingID") String BookingID
            , @Field("Status") String Status);

    @FormUrlEncoded
    @POST("/User/GetCurrentUserInfo")
    Response GetCurrentUserInfo(@Field("UserID") String UserID);

    @FormUrlEncoded
    @POST("/User/UpdateUserProfilePicture")
    Response UpdateUserProfilePicture(@Field("UserID") String UserID
            , @Field("ImageData") String ImageData);

    @FormUrlEncoded
    @POST("/User/UpdateUserInfo")
    Response UpdateUserInfo(@Field("UserID") String UserID
            , @Field("FirstName") String FirstName
            , @Field("LastName") String LastName
            , @Field("MobileNumber") String MobileNumber
            , @Field("EmailAddress") String EmailAddress
            , @Field("MaxBookings") String MaxBookings
            , @Field("ReceiveNotifications") String ReceiveNotifications
            , @Field("ReceiveOffers") String ReceiveOffers);

    @FormUrlEncoded
    @POST("/User/UpdateUserPassword")
    Response UpdateUserPassword(@Field("UserID") String UserID
            , @Field("Password") String Password);

    @FormUrlEncoded
    @POST("/User/GetUserIsStylist")
    Response GetUserIsStylist(@Field("UserID") String UserID);

    @FormUrlEncoded
    @POST("/User/GetUserLoyaltyPoints")
    Response GetUserLoyaltyPoints(@Field("UserID") String UserID);

    @FormUrlEncoded
    @POST("/User/GetUserVouchers")
    Response GetUserVouchers(@Field("UserID") String UserID);

    @FormUrlEncoded
    @POST("/User/UserScanVoucherRedeem")
    Response UserScanVoucherRedeem(@Field("UserID") String UserID
            , @Field("QRGuid") String QRGuid);

    @FormUrlEncoded
    @POST("/User/GetUserNotifications")
    Response GetUserNotifications(@Field("UserID") String UserID);

    @FormUrlEncoded
    @POST("/User/MarkNotificationAsRead")
    Response MarkNotificationAsRead(@Field("NotificationID") String NotificationID);

    @FormUrlEncoded
    @POST("/User/DeleteNotification")
    Response DeleteNotification(@Field("NotificationID") String NotificationID);

    @FormUrlEncoded
    @POST("/User/UserCheckInOut")
    Response UserCheckInOut(@Field("UserID") String UserID
            , @Field("QRGuid") String QRGuid);




}
