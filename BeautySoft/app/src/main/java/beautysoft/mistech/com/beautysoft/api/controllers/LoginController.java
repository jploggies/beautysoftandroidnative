package beautysoft.mistech.com.beautysoft.api.controllers;

import android.content.Context;

import org.json.JSONException;

import beautysoft.mistech.com.beautysoft.api.Controller;
import beautysoft.mistech.com.beautysoft.api.IControllerListener;
import retrofit.client.Response;

/**
 * Created by George on 11/21/2017.
 */

public class LoginController extends Controller {

    private static final String TAG = "CUserController";

    public LoginController(Context c, IControllerListener callback)
    {
        super(c, callback);
    }

    public Response loginUser(String UserName, String Password, String DeviceToken, String DevicePlatform, String RegistrationID) throws JSONException
    {
        Response response;
        try {
            response = getClient().getLoginService().LoginUser(UserName
                    , Password
                    , DeviceToken
                    , DevicePlatform
                    , RegistrationID);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response VerifyUserIDAndToken(String UserID, String DeviceToken, String DevicePlatform, String RegistrationID) throws JSONException
    {
        Response response;
        try {
            response = getClient().getLoginService().VerifyUserIDAndToken(UserID
                    , DeviceToken
                    , DevicePlatform
                    , RegistrationID);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response RegisterUser(String UserName,String FirstName,String LastName,String Password,String Email,String CellNo,String DeviceToken,String DevicePlatform, String RegistrationID, String Gender) throws JSONException
    {
        Response response;
        try {
            response = getClient().getLoginService().RegisterUser(UserName
                    , FirstName
                    , LastName
                    , Password
                    , Email
                    , CellNo
                    , DeviceToken
                    , DevicePlatform
                    , RegistrationID
                    , Gender);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response ForgotPassword(String UserName,String Email) throws JSONException
    {
        Response response;
        try {
            response = getClient().getLoginService().ForgotPassword(UserName
                    , Email);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response GetUserNameNotUsed(String UserName) throws JSONException
    {
        Response response;
        try {
            response = getClient().getLoginService().GetUserNameNotUsed(UserName);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Response GetEmailAddressNotUsed(String EmailAddress) throws JSONException
    {
        Response response;
        try {
            response = getClient().getLoginService().GetEmailAddressNotUsed(EmailAddress);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
