package beautysoft.mistech.com.beautysoft.common;

import java.io.IOException;

/**
 * Created by George on 11/21/2017.
 */

public class NoConnectivityException extends IOException {

    @Override
    public String getMessage() {
        return "No network available, please check your WiFi or Data connection";
    }
}
